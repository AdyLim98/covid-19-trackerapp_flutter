import 'package:corona/datasource.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class InfoPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child:Column(children: [
        GestureDetector(
          onTap:(){
            launch('https://www.who.int/emergencies/diseases/novel-coronavirus-2019/question-and-answers-hub/q-a-detail/coronavirus-disease-covid-19-how-is-it-transmitted');
          },
          child:Container(
          margin:EdgeInsets.symmetric(vertical:10),
          padding:EdgeInsets.symmetric(vertical:10,horizontal:10),
          color:primaryPurple,
          child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('FAQS',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",fontSize: 15)),
              Icon(Icons.arrow_forward,color:Colors.white,size: 14,)
            ],
          ))
        ),
        GestureDetector(
          onTap:(){
            launch('https://covid19responsefund.org/');
          },
          child:Container(
            // margin:EdgeInsets.symmetric(vertical:10),
            padding:EdgeInsets.symmetric(vertical:10,horizontal:10),
            color:primaryPurple,
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('DONATE',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontFamily: "OpenSansBold",fontSize: 15)),
                Icon(Icons.arrow_forward,color:Colors.white,size: 14,)
              ],
            )
          )
        )
      ],)
    );
  }
}