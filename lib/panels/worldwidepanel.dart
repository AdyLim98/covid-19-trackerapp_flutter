import 'package:corona/datasource.dart';
import 'package:corona/world_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WorldWidePanel extends StatelessWidget{
  
  World world;
  WorldWidePanel({Key key, this.world}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return Container(
      child:GridView(
        shrinkWrap: true,
        //whole page is scrollable but only grid is not scrollable
        physics:NeverScrollableScrollPhysics(),
        //TWICE time of the height compare to width ,then we will get nice rectangle here
        //crossAxisCount :2 means there is 2 object/things/material in a row WHILE childAspectRatio 2 means is the ratio of the width/height size
        gridDelegate:SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,childAspectRatio: 2),
        children: [
          StatusPanel(
            title:"CONFIRMED",
            panelColor: Colors.red[100],
            textColor:Colors.red,
            count:world.cases.toString(),
          ),
          StatusPanel(
            title:"ACTIVE",
            panelColor: Colors.blue[100],
            textColor:Colors.red,
            count:world.active.toString(),
          ),
          StatusPanel(
            title:"RECOVERED",
            panelColor: Colors.green[100],
            textColor:Colors.red,
            count:world.recovered.toString(),
          ),
          StatusPanel(
            title:"DEATHS",
            panelColor: Colors.grey[400],
            textColor:Colors.red,
            count:world.deaths.toString(),
          ),
        ],
      )
    );

  }
}

//stless / stf
class StatusPanel extends StatelessWidget {

  final Color panelColor;
  final Color textColor;
  final String title;
  final String count;

  const StatusPanel({Key key, this.panelColor, this.textColor, this.title, this.count}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      margin:EdgeInsets.all(10),
      height:80,
      width: width/2,
      color:panelColor,
      child:Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(title,style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,fontFamily: "Langar",color:Colors.white)),
          Text(count,style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,fontFamily: "Langar",color:Colors.white))
        ],
      )
    );
  }
}