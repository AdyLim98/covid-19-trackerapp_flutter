import 'package:corona/countries_model.dart';
import 'package:flutter/material.dart';

import '../world_model.dart';

class MostAffectCountries extends StatelessWidget {
  List <Countries> countryData;
  MostAffectCountries({Key key, this.countryData}) : super(key: key);

  // final List<String> entries = <String>['A', 'B', 'C'];
  // final List<int> colorCodes = <int>[600, 500, 100];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(child:Container(
      
      // child:ListView.builder(
      //   shrinkWrap: true,
      //   padding: const EdgeInsets.all(8),
      //   itemCount: entries.length,
      //   itemBuilder: (BuildContext context, int index) {
      //     return Container(
      //       height: 50,
      //       color: Colors.amber[colorCodes[index]],
      //       child: Center(child: Text('Entry ${entries[index]}')),
      //     );
      //   }
      // )
      child:ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount:5,
        itemBuilder: (context,index){
          return Container(
          margin:EdgeInsets.symmetric(horizontal:10,vertical: 10),
          child:Row(
            children: [
              Image.network(countryData[index].flag,height:30),
              SizedBox(width:10),
              Text(countryData[index].country, style:TextStyle(fontFamily: "OpenSansBold")),
              SizedBox(width:10),
              Text("Deaths:" + countryData[index].deaths.toString() ,style:TextStyle(color: Colors.red,fontFamily: "OpenSansBold")),
            ],
          )
        );
      },
    )
      // child:Text("Im here")
       
    ));
  }
}