import 'package:corona/datasource.dart';
import 'package:corona/homepage.dart';
import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      fontFamily:'Langar',
      primaryColor: primaryPurple,
    ),
    home:HomePage(),
  ));
}