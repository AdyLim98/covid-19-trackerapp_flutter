import 'dart:convert';
import 'dart:io';
import 'package:corona/pages/countrypage.dart';
import 'package:corona/pages/malaysiapage.dart';
import 'package:corona/panels/infoPanel.dart';
import 'package:corona/panels/mostaffectedcountries.dart';
import 'package:corona/world_model.dart';
import 'package:logger/logger.dart';

import 'package:corona/datasource.dart';
import 'package:corona/panels/worldwidepanel.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

import 'countries_model.dart';
import 'httpService.dart';
import 'malaysia_model.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final HttpService httpService = HttpService();
 
  // Map worldData;
  // fetchWorldData() async{
  //   http.Response response = await http.get('http://disease.sh/v3/covid-19/all');

  //   setState((){
  //     worldData = json.decode(response.body);
  //   });
  // }
  World _world;
  List <Countries> countries;
  Malaysia malaysia;
  
  @override
  void initState(){
    // fetchWorldData();
    // final logger = Logger();
    // logger.d(worldData);
    print("222232");
    httpService.getCovidData().then((s)=>setState((){
      _world = s;
    }));
    httpService.getCountriesData().then((country)=>setState((){
      countries = country;
    }));
    httpService.getMalaysiaData().then((my)=>setState((){
      malaysia = my;
    }));
    super.initState();

    print(_world);
  }

  void showConsoleUsingPrint() {
    // print(httpService.getWorldCorona());
    // print(worldData);
    print(countries);
  }
 
   void showConsoleUsingDebugPrint() {
 
    debugPrint('Console Message Using Debug Print');
 
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title:Text("Covid-19 Tracker App")
      ),
      body:SingleChildScrollView(child:Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding:EdgeInsets.fromLTRB(20, 20, 20, 20),
            height:120,
            color:Colors.orange[100],
            child:Text(DataSource.quote,style:TextStyle(color:Colors.red,fontFamily: "OpenSansBold",fontSize: 15))
          ),
          Padding(
            padding:const EdgeInsets.all(8.0),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children:[
                Text("Whole World",style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold ,fontFamily: "OpenSansBold")),
                // Padding(
                  // padding:const EdgeInsets.fromLTRB(40,0,0,0),
                   GestureDetector(
                    onTap:(){
                      Navigator.push(context,MaterialPageRoute(builder: (context)=>CountryPage(countryData:countries)));
                    },
                   child:Container(
                    decoration: BoxDecoration(
                      color: primaryPurple,
                      borderRadius: BorderRadius.circular(10) 
                    ),
                    padding:EdgeInsets.all(10),
                    child:Text("All",style:TextStyle(fontSize: 15,color:Colors.white ,fontFamily: "OpenSansBold")),
                ))
                // )
              ]
            )
          ),
          //the left hand side world is follow the word from the constructor in worldwidepanel
          (_world==null)?Center(child:CircularProgressIndicator()): WorldWidePanel(world:_world),
          // Center(child:(CircularProgressIndicator())),
          Padding(
            padding:const EdgeInsets.all(8.0),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children:[
              Text("Countries",style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold ,fontFamily: "OpenSansBold")),
              GestureDetector(
                onTap:(){
                  Navigator.push(context,MaterialPageRoute(builder: (context)=>MalaysiaPage(malaysia:malaysia)));
                },
                child:Container(
                    decoration: BoxDecoration(
                      color: primaryPurple,
                      borderRadius: BorderRadius.circular(10) 
                    ),
                    padding:EdgeInsets.all(10),
                    child:Text("Malaysia--->",style:TextStyle(fontSize: 15,color:Colors.white ,fontFamily: "OpenSansBold")),
                ))
            ])
          ),
          (countries==null)?Center(child:CircularProgressIndicator()):MostAffectCountries(countryData:countries),
          InfoPanel(),
          //future builder is used to get the data from api
          // FutureBuilder<List<Countries>>(
          // future:httpService.getCountriesData(),
          // builder: (context,snapshot){
          //   if(snapshot.hasData){
          //     final data = snapshot.data;
          //     return Text("Names:"+data.toString());
          //   }else if(snapshot.hasError){
          //     return Text(snapshot.error.toString());
          //   }
          //   return CircularProgressIndicator();
          // },
          // ),

          //Use the button for test debug
          //  RaisedButton(
          //     onPressed: () => showConsoleUsingPrint(),
          //     child: Text(' Print Console Message using Print '),
          //     textColor: Colors.white,
          //     color: Colors.green,
          //     padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
          //   )
        ],
      ))
    );
  }
}

// Future <World> getCovidData() async{
//   final url="https://disease.sh/v3/covid-19/all";
//   final response = await http.get(url);
  
//     if(response.statusCode==200){
//       //can define as final or  Map<String,dynamic> for the json data
//       final jsonData = jsonDecode(response.body);
//       return World.fromJson(jsonData);
//     }else{
//       throw Exception();
//     }
//   }
  
