import 'dart:convert';
import 'package:corona/countries_model.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'malaysia_model.dart';
import 'world_model.dart';

class HttpService{
 Future <World> getCovidData() async{
  final url="https://disease.sh/v3/covid-19/all";
  final response = await http.get(url);
  
    if(response.statusCode==200){
      //can define as final or  Map<String,dynamic> for the json data
      final jsonData = jsonDecode(response.body);
      return World.fromJson(jsonData);
    }else{
      throw Exception();
    }
  }

  Future <List<Countries>> getCountriesData() async{
  final url="https://disease.sh/v3/covid-19/countries";
  final response = await http.get(url);
  
    if(response.statusCode==200){
      //can define as final or  Map<String,dynamic> for the json data
      List jsonData = jsonDecode(response.body);
      return jsonData.map((data) => new Countries.fromJson(data)).toList();
  
      // return Countries.fromJson(jsonData[0]);
    }else{
      throw Exception();
    }
  }

  Future <Malaysia> getMalaysiaData() async{
  final url="https://disease.sh/v3/covid-19/countries/Malaysia";
  final response = await http.get(url);
  
    if(response.statusCode==200){
      //can define as final or  Map<String,dynamic> for the json data
      final jsonData = jsonDecode(response.body);
      return Malaysia.fromJson(jsonData);
    }else{
      throw Exception();
    }
  }
}
